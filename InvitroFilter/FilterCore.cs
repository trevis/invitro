﻿using System;
using System.IO;
using System.Runtime.InteropServices;


using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Decal.Filters;
using InVitroFilter.Lib;

namespace InVitroFilter {
    [FriendlyName("InVitroFilter")]
    public class FilterCore : FilterBase {
        internal static string PluginName = "InVitroFilter";

        internal static CharacterCreator characterCreator = new CharacterCreator();
        private bool didGoodLogin = false;

        internal static CharacterTrainer characterTrainer;

        protected override void Startup() {
            //Util.SetPluginName(PluginName);
            Util.CreateDataDirectories();
            Config.Reload();
            ServerDispatch += FilterCore_ServerDispatch;
            CoreManager.Current.CommandLineText += Current_CommandLineText;
            characterCreator.Start();
        }

        protected override void Shutdown() {
            ServerDispatch -= FilterCore_ServerDispatch;
            CoreManager.Current.CommandLineText -= Current_CommandLineText;
            CoreManager.Current.CharacterFilter.LoginComplete -= CharacterFilter_LoginComplete;
            characterCreator.Stop();
        }

        private void Current_CommandLineText(object sender, ChatParserInterceptEventArgs e) {
            try {
                if (e.Text == "/iv logout") {
                    e.Eat = true;
                    CoreManager.Current.Actions.Logout();
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            try {
                Util.WriteToDebugLog("CharacterFilter_LoginComplete");
                if (didGoodLogin) return;

                var wo = CoreManager.Current.WorldFilter[CoreManager.Current.CharacterFilter.Id];

                if (wo == null) {
                    Util.WriteToDebugLog("wo is null for " + CoreManager.Current.CharacterFilter.Id.ToString());
                    return;
                }

                didGoodLogin = true;

                var name = wo.Name.Replace("+","");
                Egg egg = null;

                if (characterCreator.egg != null) {
                    egg = characterCreator.egg;
                }
                else {
                   egg = Egg.FromServerCharacter(CoreManager.Current.CharacterFilter.Server, CoreManager.Current.CharacterFilter.AccountName, name);
                }

                if (egg != null && !string.IsNullOrEmpty(egg.LevelingTemplate)) {
                    Util.WriteToDebugLog("Got Egg: " + name + ": " + egg.LevelingTemplate);
                    characterTrainer = new CharacterTrainer(egg);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        void FilterCore_ServerDispatch(object sender, NetworkMessageEventArgs e) {
            try {
                if (e.Message.Type == 0xF7DF) {
                    Util.WriteToDebugLog("Got 0xF7DF");
                    CoreManager.Current.CharacterFilter.LoginComplete += CharacterFilter_LoginComplete;
                }

                characterCreator.FilterCore_ServerDispatch(sender, e);

                if (characterTrainer != null) {
                    //characterTrainer.FilterCore_ServerDispatch(sender, e);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}
