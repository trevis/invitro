﻿using Decal.Adapter;
using InVitroFilter.Lib;
using InVitroFilter.Lib.CharacterScreens;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Text;

namespace InVitroFilter {
    class CharacterCreator {
        uint characterCount;
        string accountName = "Unknown";
        string serverName = "Unknown";
        private bool needsEggCheck;
        private bool gotCharacterList = false;
        private bool gotServerName = false;
        DateTime gotInfoAt = DateTime.MinValue;
        DateTime firstThought = DateTime.MinValue;
        bool didClickCreateCharacter = false;
        public Egg egg;

        DateTime lastThought = DateTime.UtcNow;
        int actionIndex = 0;
        private Character loginCharacter;

        List<Character> characters = new List<Character>();
        List<Character> delCharacters = new List<Character>();
        List<String> characterNames = new List<string>();

        List<string> hatchedEggNames = new List<string>();
        List<string> deletedCharacterNames = new List<string>();
        private Character deletingCharacter = null;

        public static void Skip(BinaryReader reader, uint length) { reader.BaseStream.Position += length; }

        private static uint CalculatePadMultiple(uint length, uint multiple) {
            return multiple * ((length + multiple - 1u) / multiple) - length;
        }

        internal void FilterCore_ServerDispatch(object sender, NetworkMessageEventArgs e) {
            try {
                if (e.Message.Type == 0xF658) {
                    characters.Clear();
                    characterNames.Clear();

                    Stream stream = new MemoryStream(e.Message.RawData);
                    var reader = new BinaryReader(stream);

                    var status = reader.ReadUInt32();
                    characterCount = reader.ReadUInt32();
                    characterCount = reader.ReadUInt32();
                    
                    ushort length;
                    string rdrStr;

                    for (var i = 0; i < characterCount; i++) {
                        var id = (int)reader.ReadUInt32();

                        length = reader.ReadUInt16();
                        rdrStr = (length != 0 ? new string(reader.ReadChars(length)) : string.Empty);

                        // client pads string length to be a multiple of 4 including the 2 bytes for length
                        Skip(reader, CalculatePadMultiple(sizeof(ushort) + (uint)length, 4u));
                        var name = rdrStr.Replace("+", "");

                        var delTimeout = (int)reader.ReadUInt32();

                        if (delTimeout == 0) {
                            characters.Add(new Character(id, name, delTimeout, (int)characterCount-i));
                            characterNames.Add(name);
                        }
                    }

                    characters.Sort((a, b) => String.Compare(a.Name, b.Name, StringComparison.Ordinal));

                    var x = 0;
                    foreach (var character in characters) {
                        character.Slot = x;
                        x++;
                    }

                    var unknown2 = reader.ReadUInt32();
                    var numAllowedCharacters = reader.ReadInt32();
                    length = reader.ReadUInt16();
                    rdrStr = (length != 0 ? new string(reader.ReadChars(length)) : string.Empty);

                    // client pads string length to be a multiple of 4 including the 2 bytes for length
                    Skip(reader, CalculatePadMultiple(sizeof(ushort) + (uint)length, 4u));
                    accountName = rdrStr;



                    if (characterCount == numAllowedCharacters) {
                        // we cant do anything yet, full up
                        return;
                    }

                    gotCharacterList = true;
                }

                if (e.Message.Type == 0xF7E1) {
                    serverName = Convert.ToString(e.Message["server"]);
                    gotServerName = true;

                    CoreManager.Current.RenderFrame += Current_RenderFrame;
                }

                if (gotServerName && gotCharacterList && gotInfoAt == DateTime.MinValue) {
                    needsEggCheck = true;
                    gotInfoAt = DateTime.UtcNow;
                    gotCharacterList = false;
                    gotServerName = false;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public void Start() {
        }

        public void Stop() {
            CoreManager.Current.RenderFrame -= Current_RenderFrame;
        }

        public string[] GetEggFiles() {
            return Directory.GetFiles(Util.GetEggsDirectory(serverName, accountName), "*.json");
        }

        public void CheckEggs() {
            if (Directory.Exists(Util.GetEggsDirectory(serverName, accountName))) {
                string[] eggs = GetEggFiles();

                foreach (var egg in eggs) {
                    var parts = egg.Split('\\');
                    var eggName = parts[parts.Length - 1].Replace(".json", "");

                    var eggInstance = Egg.FromJSON(eggName, File.ReadAllText(egg));

                    Util.WriteToDebugLog("Check egg: " + eggName + " name: " + eggInstance.Name);

                    if (eggInstance.RepeatedlyDelete && characterNames.Contains(eggName) && !deletedCharacterNames.Contains(eggName)) {
                        DeleteCharacter(eggName, eggInstance);
                        return;
                    }
                }

                foreach (var egg in eggs) {
                    var parts = egg.Split('\\');
                    var eggName = parts[parts.Length - 1].Replace(".json", "");

                    if (!characterNames.Contains(eggName)) {
                        HatchEgg(eggName);
                        return;
                    }
                }
            }
        }

        private void EnsureVGITrackingEnabled(string characterName, string server, Egg eggInstance) {
            try {
                var dbPath = @"C:\Games\VirindiPlugins\VirindiGlobalInventory\_" + server + ".db";

                if (!eggInstance.EnableVGITracking) return;

                var m_dbConnection = new SQLiteConnection($"Data Source={dbPath};Version=3;");
                m_dbConnection.Open();

                try {
                    var deleteCommand = new SQLiteCommand("delete from CharactersEnabled where CharServer=@Server and CharName=@Character", m_dbConnection);
                    deleteCommand.Parameters.AddWithValue("Character", characterName);
                    deleteCommand.Parameters.AddWithValue("Server", server);
                    deleteCommand.ExecuteNonQuery();

                    var insertCommand = new SQLiteCommand("insert into CharactersEnabled (CharServer,CharName,Tracking) values(@Server,@Character,3)", m_dbConnection);
                    insertCommand.Parameters.AddWithValue("Character", characterName);
                    insertCommand.Parameters.AddWithValue("Server", server);
                    insertCommand.ExecuteNonQuery();

                    Util.WriteToChat("Force enabling VGI Tracking for this character");
                }
                catch (Exception ex) { Util.LogException(ex); }

                m_dbConnection.Close();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        static bool DoesCharacterOwnItems(string characterName, string server, Egg eggInstance) {
            try {
                var hasItems = false;
                var dbPath = @"C:\Games\VirindiPlugins\VirindiGlobalInventory\_" + server + ".db";

                if (eggInstance.DontDeleteIfOwnsItems.Count == 0) return false;

                var m_dbConnection = new SQLiteConnection($"Data Source={dbPath};Version=3;");
                m_dbConnection.Open();

                try {
                    var command = new SQLiteCommand("select * from ObjectData where OwnerServer=@Server and OwnerCharName=@Character", m_dbConnection);
                    command.Parameters.AddWithValue("Character", characterName);
                    command.Parameters.AddWithValue("Server", server);
                    SQLiteDataReader reader = command.ExecuteReader();

                    while (reader.Read()) {
                        if (eggInstance.DontDeleteIfOwnsItems.Contains(reader["ObjectName"].ToString())) {
                            hasItems = true;
                            break;
                        }
                    }
                }
                catch (Exception ex) { Util.LogException(ex); }

                m_dbConnection.Close();

                return hasItems;
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        private void DeleteCharacter(string characterName, Egg eggInstance) {
            try {
                Util.egg = characterName;

                foreach (var character in characters) {
                    if (character.Name == characterName) {
                        if (DoesCharacterOwnItems(characterName, serverName, eggInstance)) {
                            loginCharacter = character;
                            egg = eggInstance;
                            egg.NeedsToReturnItems = true;
                            egg.screens.Clear();
                            egg.screens.Add(new FinishScreen(egg));

                            Util.WriteToDebugLog("Skipping delete because owns items!!!");
                        }
                        else {
                            deletingCharacter = character;
                            Util.WriteToDebugLog("Deleting character");
                        }

                        needsEggCheck = false;

                        CoreManager.Current.RenderFrame += Current_RenderFrame;
                        break;
                    }
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void HatchEgg(string eggName) {
            var eggPath = Path.Combine(Util.GetEggsDirectory(serverName, accountName), $"{eggName}.json");
            var jsonText = System.IO.File.ReadAllText(eggPath);
            Util.egg = eggName;
            Util.WriteToDebugLog("Hatching egg: " + eggPath);
            egg = Egg.FromJSON(eggName, jsonText);
            egg.Name = eggName;
            egg.Server = serverName;
            egg.Account = accountName;
            egg.Path = eggPath;
            firstThought = DateTime.UtcNow;
            didClickCreateCharacter = false;
            gotInfoAt = DateTime.MinValue;

            EnsureVGITrackingEnabled(eggName, serverName, egg);

            CoreManager.Current.RenderFrame += Current_RenderFrame;
        }

        private void Current_RenderFrame(object sender, EventArgs e) {
            try {
                Think();
            }
            catch (Exception ex) {
                Util.LogException(ex);
                CoreManager.Current.RenderFrame -= Current_RenderFrame;
            }
        }

        public void Think() {
            if (needsEggCheck && DateTime.UtcNow - gotInfoAt > TimeSpan.FromMilliseconds(800)) {
                needsEggCheck = false;
                CoreManager.Current.RenderFrame -= Current_RenderFrame;
                CheckEggs();
                return;
            }

            if (loginCharacter != null) {
                if (DateTime.UtcNow - lastThought < TimeSpan.FromMilliseconds(200)) return;

                lastThought = DateTime.UtcNow;

                switch (actionIndex) {
                    case 0:
                        loginCharacter.Click();
                        break;

                    case 1:
                        InputManager.MouseClick(340, 420); // click enter world
                        break;

                    default:
                        loginCharacter = null;
                        actionIndex = 0;
                        CoreManager.Current.RenderFrame -= Current_RenderFrame;
                        break;
                }

                actionIndex++;

                return;
            }

            if (deletingCharacter != null) {
                if (DateTime.UtcNow - lastThought < TimeSpan.FromMilliseconds(200)) return;

                lastThought = DateTime.UtcNow;

                switch (actionIndex) {
                    case 0:
                        deletingCharacter.Click();
                        break;

                    case 1:
                        InputManager.MouseClick(124, 582); // click delete button
                        break;

                    case 2:
                        InputManager.SendKeys("delete");
                        break;

                    case 3:
                        InputManager.MouseClick(321, 380); // click ok
                        break;

                    case 4:
                        break;
                }

                actionIndex++;

                if (actionIndex > 5) {
                    deletedCharacterNames.Add(deletingCharacter.Name);
                    characterNames.Remove(deletingCharacter.Name);
                    deletingCharacter = null;
                    actionIndex = 0;
                    CoreManager.Current.RenderFrame -= Current_RenderFrame;
                    CheckEggs();
                }

                return;
            }

            if (egg == null) {
                return;
            }

            if (egg.IsHatched()) {
                Util.WriteToDebugLog("Remove renderframe from hatched egg");
                CoreManager.Current.RenderFrame -= Current_RenderFrame;

                if (egg.didError) {
                    egg = null;
                    CheckEggs();
                }
                else {
                    hatchedEggNames.Add(egg.Name);
                    egg = null;
                }

                return;
            }

            if (!didClickCreateCharacter && DateTime.UtcNow - firstThought > TimeSpan.FromMilliseconds(200)) {
                Util.WriteToDebugLog($"Clicking Create Character.");
                InputManager.MouseClick(337, 241);
                didClickCreateCharacter = true;
                egg.WriteToLog();
                egg.Hatch();
                return;
            }

            if (!didClickCreateCharacter) return;

            egg.Think();
        }
    }
}
