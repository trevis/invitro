﻿using InVitroFilter.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace InVitroFilter {
    static class Config {
        public static string DefaultStartingTown = "holtburg";
        public static List<string> DefaultLoginCommands = new List<string> { "/mt logout" };

        public static void Reload() {
            var configPath = Path.Combine(Util.GetDataDirectory(), "config.xml");

            if (!File.Exists(configPath)) return;

            XmlDocument doc = new XmlDocument();
            doc.Load(configPath);

            foreach (XmlNode node in doc.DocumentElement.ChildNodes) {
                try {
                    switch (node.Name) {
                        case "DefaultStartingTown":
                            DefaultStartingTown = node.InnerText.ToString().Trim();
                            break;

                        case "DefaultLoginCommands":
                            DefaultLoginCommands.Clear();
                            foreach (XmlNode childNode in node.ChildNodes) {
                                DefaultLoginCommands.Add(childNode.InnerText.ToString().Trim());
                            }
                            break;
                    }
                }
                catch (Exception ex) { Util.LogException(ex); }
            }
        }
    }
}
