﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Decal.Filters;
using InVitroFilter.Lib;
using InVitroFilter.Lib.Constants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace InVitroFilter {

    class CharacterTrainer {
        Egg egg = null;

        List<CharacterTemplate> templates = new List<CharacterTemplate>();
        CharacterTemplate currentTemplate = null;
        XPTargetType targetType;
        int target = -1;
        int level = 0;
        bool waitingOnResponse = false;

        bool isRunning = false;
        List<string> loginCommands = new List<string>();

        DateTime lastThought = DateTime.UtcNow;
        DateTime lastAction = DateTime.UtcNow;

        public CharacterTrainer(Egg egg) {
            this.egg = egg;
            Util.egg = egg.Name;

            LoadTemplates();
            ChooseBestTemplate();

            Util.WriteToDebugLog("needsItemsReturned? " + egg.NeedsToReturnItems.ToString());

            if (egg.NeedsToReturnItems) {
                loginCommands = egg.OwnsItemsRecoveryCommands;
            }

            if (currentTemplate != null) {
                CoreManager.Current.RenderFrame += Current_RenderFrame;
                CoreManager.Current.CharacterFilter.ChangeExperience += CharacterFilter_ChangeExperience;
                CoreManager.Current.ChatBoxMessage += Current_ChatBoxMessage;

                DoWork();
            }
        }

        private Regex skillRaiseRegex = new Regex(@"^Your .* is now \d+");
        private void Current_ChatBoxMessage(object sender, ChatTextInterceptEventArgs e) {
            try {
                if (e.Color == 13 && skillRaiseRegex.IsMatch(e.Text)) {
                    waitingOnResponse = false;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void CharacterFilter_ChangeExperience(object sender, ChangeExperienceEventArgs e) {
            try {
                if (isRunning) return;
                DoWork();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void ChooseBestTemplate() {
            try {
                bool didChange = false;

                templates.Sort(new Comparison<CharacterTemplate>((x, y) => x.Level.CompareTo(y.Level)));

                foreach (var template in templates) {
                    var myLevel = CoreManager.Current.CharacterFilter.Level;

                    if (template.Level >= myLevel) {
                        if (currentTemplate == null || template.Level != currentTemplate.Level) {
                            didChange = true;
                        }
                        currentTemplate = template;
                        break;
                    }
                }

                if (didChange) {
                    Util.WriteToDebugLog("Changed template to: " + currentTemplate.Name);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void LoadTemplates() {
            try {
                var templateDir = Path.Combine(Util.GetTemplatesDirectory(), egg.LevelingTemplate);

                if (!Directory.Exists(templateDir)) {
                    Util.WriteToDebugLog("Template directory not found: " + templateDir);
                    return;
                }

                var files = Directory.GetFiles(templateDir, "*.json");

                templates.Clear();
                currentTemplate = null;
                foreach (var file in files) {
                    LoadTemplate(file);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void LoadTemplate(string file) {
            try {
                var parts = file.Split('\\');
                var eggName = parts[parts.Length - 1].Replace(".json", "");
                var template = CharacterTemplate.FromJSON(file, File.ReadAllText(file));

                templates.Add(template);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public bool DoWork() {
            try {
                // when we level, we should check to see if there is a better template to be using.
                if (level != CoreManager.Current.CharacterFilter.Level) {
                    level = CoreManager.Current.CharacterFilter.Level;
                    ChooseBestTemplate();
                }

                if (currentTemplate == null) {
                    Util.WriteToChat("currentTemplate is null!");
                    return false;
                }

                long xpToSpend = 0;
                long xpToSpendTen = 0;
                long xpToSpendOne = 0;
                int raiseAmount = 1;
                int targetClicks = 0;

                foreach (var attribute in currentTemplate.AttributeTargets) {
                    var clicks = CoreManager.Current.Actions.AttributeClicks[attribute.Key];
                    if (attribute.Value <= 0 || attribute.Value <= clicks) {
                        continue;
                    }

                    var cost = Util.GetXPNeededToRaiseAttribute(attribute.Key, 1);

                    if (cost > 0 && CoreManager.Current.CharacterFilter.UnassignedXP >= cost && (cost < xpToSpendOne || xpToSpendOne == 0)) {
                        xpToSpend = cost;
                        xpToSpendOne = cost;

                        var costTen = Util.GetXPNeededToRaiseAttribute(attribute.Key, 10);

                        xpToSpendTen = costTen;

                        if (costTen > 0 && costTen <= CoreManager.Current.CharacterFilter.UnassignedXP && attribute.Value >= clicks+10) {
                            xpToSpend = costTen;
                            raiseAmount = 10;
                        }

                        targetType = XPTargetType.Attribute;
                        target = (int)attribute.Key;
                        targetClicks = attribute.Value;
                    }
                }

                foreach (var vital in currentTemplate.VitalTargets) {
                    var clicks = CoreManager.Current.Actions.VitalClicks[vital.Key];
                    if (vital.Value <= 0 || vital.Value <= CoreManager.Current.Actions.VitalClicks[vital.Key]) {
                        continue;
                    }

                    var cost = Util.GetXPNeededToRaiseVital(vital.Key, 1);

                    if (cost > 0 && CoreManager.Current.CharacterFilter.UnassignedXP >= cost && (cost < xpToSpendOne || xpToSpendOne == 0)) {
                        xpToSpend = cost;
                        xpToSpendOne = cost;
                        var costTen = Util.GetXPNeededToRaiseVital(vital.Key, 10);
                        xpToSpendTen = costTen;

                        if (costTen > 0 && costTen <= CoreManager.Current.CharacterFilter.UnassignedXP && vital.Value >= clicks+10) {
                            xpToSpend = costTen;
                            raiseAmount = 10;
                        }

                        targetType = XPTargetType.Vital;
                        target = (int)vital.Key;
                        targetClicks = vital.Value;
                    }
                }

                foreach (var skill in currentTemplate.SkillTargets) {
                    var clicks = CoreManager.Current.Actions.SkillClicks[skill.Key];
                    if (skill.Value <= 0 || skill.Value <= CoreManager.Current.Actions.SkillClicks[skill.Key]) {
                        continue;
                    }

                    if (CoreManager.Current.Actions.SkillTrainLevel[skill.Key] < 2) continue;

                    var cost = Util.GetXPNeededToRaiseSkill(skill.Key, 1);

                    if (cost > 0 && CoreManager.Current.CharacterFilter.UnassignedXP >= cost && (cost < xpToSpendOne || xpToSpendOne == 0)) {
                        xpToSpend = cost;
                        xpToSpendOne = cost;
                        var costTen = Util.GetXPNeededToRaiseSkill(skill.Key, 10);
                        xpToSpendTen = costTen;

                        if (costTen > 0 && costTen <= CoreManager.Current.CharacterFilter.UnassignedXP && skill.Value >= clicks+10) {
                            xpToSpend = costTen;
                            raiseAmount = 10;
                        }

                        targetType = XPTargetType.Skill;
                        target = (int)skill.Key;
                        targetClicks = skill.Value;
                    }
                }

                if (xpToSpend == 0) {
                    return false;
                }

                isRunning = true;


                lastAction = DateTime.UtcNow;
                waitingOnResponse = true;

                if (targetType == XPTargetType.Attribute) {
                    Util.WriteToDebugLog($"Cheapest is {(AttributeType)target} doClicks:{raiseAmount} cost:{xpToSpend} targetClicks:{targetClicks} costTen:{xpToSpendTen}");
                    var t = (AttributeType)Enum.Parse(typeof(AttributeType), ((AttributeType)target).ToString().Replace("Base", "Current"));
                    CoreManager.Current.Actions.AddAttributeExperience((AttributeType)t, (int)xpToSpend);
                    return true;
                }
                else if (targetType == XPTargetType.Skill) {
                    Util.WriteToDebugLog($"Cheapest is {(Decal.Adapter.Wrappers.SkillType)target} doClicks:{raiseAmount} cost:{xpToSpend} targetClicks:{targetClicks} costTen:{xpToSpendTen}");
                    var t = (Decal.Adapter.Wrappers.SkillType)Enum.Parse(typeof(Decal.Adapter.Wrappers.SkillType), ((Decal.Adapter.Wrappers.SkillType)target).ToString().Replace("Base", "Current"));
                    var type = (Decal.Adapter.Wrappers.SkillType)target;
                    CoreManager.Current.Actions.AddSkillExperience(t, (int)xpToSpend);
                    return true;
                }
                else {
                    Util.WriteToDebugLog($"Cheapest is {(VitalType)target} doClicks:{raiseAmount} cost:{xpToSpend} targetClicks:{targetClicks} costTen:{xpToSpendTen}");
                    var t = (VitalType)Enum.Parse(typeof(VitalType), ((VitalType)target).ToString().Replace("Base", "Maximum"));
                    CoreManager.Current.Actions.AddVitalExperience((VitalType)t, (int)xpToSpend);
                    return true;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        private void Current_RenderFrame(object sender, EventArgs e) {
            try {
                if (loginCommands.Count > 0) {
                    var command = loginCommands[0].Replace("<[RANDOM]>", DateTime.UtcNow.ToFileTimeUtc().ToString());
                    Util.AddToChatBox(command);
                    Util.WriteToDebugLog("Issuing recovery login command: " + command);
                    loginCommands.RemoveAt(0);
                    return;
                }

                var actionReady = DateTime.UtcNow - lastAction > TimeSpan.FromMilliseconds(300);

                if (waitingOnResponse || !actionReady) return;
                
                if (isRunning && !DoWork()) {
                    isRunning = false;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}
