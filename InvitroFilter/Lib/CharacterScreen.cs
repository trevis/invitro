﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InVitroFilter.Lib {
    public abstract class CharacterScreen {
        public Egg egg;
        public bool isDone = false;
        public bool didError = false;

        public CharacterScreen(Egg egg) {
            this.egg = egg;
        }

        public abstract void Think();
        public abstract void Start();
        public abstract void Finish();
        public bool IsDone() { return isDone; }
        public bool DidError() { return didError; }
    }
}
