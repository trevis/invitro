﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace InVitroFilter.Lib {
    public static class InputManager {
        public const int WM_KEYDOWN = 0x0100;
        public const int WM_KEYUP = 0x0101;
        public const int WM_CHAR = 0x0102;

        public const int WM_MOUSEMOVE = 0x0200;
        public const int WM_LBUTTONDOWN = 0x0201;
        public const int WM_LBUTTONUP = 0x0202;

        private const byte VK_HOME = 0x24;
        private const byte VK_DELETE = 0x2E;
        private const byte VK_SHIFT = 0x10;
        private const byte VK_LSHIFT = 0xA0;
        private const byte VK_CAPITAL = 0x14;
        private const byte VK_BACK = 0x08;

        private const uint KEYEVENTF_UNICODE = 0x0004;
        public const int WM_DESTROY = 0x0002;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern bool PostMessage(IntPtr hhwnd, uint msg, IntPtr wparam, UIntPtr lparam);

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImportAttribute("User32.dll")]
        internal static extern IntPtr SetForegroundWindow(IntPtr hWnd);

        [DllImportAttribute("User32.dll")]
        internal static extern IntPtr SetBackgroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        internal static void KillClient() {
            Util.WriteToDebugLog("Killing Client");
            PostMessage(CoreManager.Current.Decal.Hwnd, WM_DESTROY, new IntPtr(0), new UIntPtr(0));
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern uint MapVirtualKey(uint uCode, uint uMapType);

        [DllImport("User32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int uMsg, int wParam, string lParam);

        public static void SendKey(IntPtr hwnd, IntPtr keyCode, bool extended, bool unicode = false) {
            uint scanCode = MapVirtualKey((uint)keyCode, 0);
            uint lParam;

            if (unicode) keyCode = (IntPtr)WM_CHAR;

            lParam = (0x00000001 | (scanCode << 16));
            if (extended) {
                lParam |= 0x01000000;
                
            }

            PostMessage(hwnd, unicode ? KEYEVENTF_UNICODE | 0 : (UInt32)WM_KEYDOWN, (IntPtr)keyCode, (UIntPtr)lParam);

            lParam |= 0xC0000000;
            PostMessage(hwnd, unicode ? KEYEVENTF_UNICODE | WM_KEYUP : WM_KEYUP, (IntPtr)keyCode, (UIntPtr)lParam);
        }

        public static void SendKeys(string keys) {
            foreach (char key in keys) {
                PostMessage(CoreManager.Current.Decal.Hwnd, WM_CHAR, (IntPtr)key, (UIntPtr)0);
            }
        }

        public static void MouseClick(int x, int y) {
            int loc = (y * 0x10000) + x;

            PostMessage(CoreManager.Current.Decal.Hwnd, WM_MOUSEMOVE, (IntPtr)0x00000000, (UIntPtr)loc);
            PostMessage(CoreManager.Current.Decal.Hwnd, WM_LBUTTONDOWN, (IntPtr)0x00000001, (UIntPtr)loc);
            PostMessage(CoreManager.Current.Decal.Hwnd, WM_LBUTTONUP, (IntPtr)0x00000000, (UIntPtr)loc);
        }

        public static void ClearTextField(int size=3) {
            SendKey(CoreManager.Current.Decal.Hwnd, (IntPtr)VK_HOME, true);

            while (size > 0) {
                SendKey(CoreManager.Current.Decal.Hwnd, (IntPtr)VK_BACK, true);
                SendKey(CoreManager.Current.Decal.Hwnd, (IntPtr)VK_DELETE, true);
                size--;
            }
        }
    }
}
