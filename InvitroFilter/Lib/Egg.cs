﻿using InVitroFilter.Lib.CharacterScreens;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace InVitroFilter.Lib {
    public class Egg {
        public JObject Data;
        public string Name = "None";
        public string Race = "Aluvian";
        public string Gender = "Male";
        public string StartingTown = "Holtburg";
        public bool RepeatedlyDelete = false;
        public bool NeedsToReturnItems = false;

        public string LevelingTemplate = "";

        public string Account { get; internal set; }

        public List<string> LoginCommands = new List<string>();
        public string Strength = "10";
        public string Endurance = "10";
        public string Coordination = "10";
        public string Quickness = "10";
        public string Focus = "10";
        public string Self = "10";

        public List<string> SpecializedSkills = new List<string>();
        public List<string> TrainedSkills = new List<string>();

        public bool EnableVGITracking = false;

        public List<string> DontDeleteIfOwnsItems = new List<string>();
        public List<string> OwnsItemsRecoveryCommands = new List<string>();

        public bool isHatched = false;
        public bool didError = false;
        public List<CharacterScreen> screens = new List<CharacterScreen>();

        public string Server { get; internal set; }
        public string Path { get; internal set; }

        public static Egg FromJSON(string name, string json) {
            try {
                var jobj = JObject.Parse(json);

                Util.WriteToDebugLog($"Parsing {name} from json {json}");

                if (jobj["template"] != null) {
                    var templatePath = System.IO.Path.Combine(Util.GetTemplatesDirectory(), jobj["template"].ToString());
                    Util.WriteToDebugLog($"template path for {name} is {templatePath}");
                    if (Directory.Exists(templatePath)) {
                        var templateFiles = Directory.GetFiles(templatePath, "*.json");
                        var lowestLevel = int.MaxValue;

                        foreach (var templateFile in templateFiles) {
                            var templateJSON = File.ReadAllText(templateFile);
                            var templateObj = JObject.Parse(templateJSON);

                            if (templateObj["level"] != null) {
                                var level = 0;

                                Util.WriteToDebugLog($"found level {templateObj["level"].ToString()}");

                                if (Int32.TryParse(templateObj["level"].ToString(), out level) && level < lowestLevel) {
                                    json = templateJSON;
                                    jobj = templateObj;
                                    lowestLevel = level;
                                    Util.WriteToDebugLog("set new template to: " + templateFile);
                                }
                            }
                        }
                    }
                    else {
                        Util.WriteToDebugLog("template path does not exist " + templatePath);
                    }
                }
                else {
                    Util.WriteToDebugLog("template config does not exist");
                }

                var egg = new Egg(jobj);
                egg.Name = name;
                return egg;
            }
            catch (Exception ex) {
                Util.LogException(ex);
                return null;
            }
        }

        public static Egg FromServerCharacter(string server, string account, string character) {
            try {
                var path = System.IO.Path.Combine(Util.GetEggsDirectory(), server);
                path = System.IO.Path.Combine(path, account);
                path = System.IO.Path.Combine(path, $"{character}.json");

                Util.WriteToDebugLog(path);

                if (!File.Exists(path)) return null;

                return FromJSON(character, File.ReadAllText(path));
            }
            catch (Exception ex) {
                Util.LogException(ex);
                return null;
            }
        }

        public Egg(JObject data) {
            try {
                Data = data;
                Name = data["name"].ToString();
                Race = data["race"].ToString();
                Gender = data["gender"].ToString();
                StartingTown = data["startingTown"] != null ? data["startingTown"].ToString() : StartingTown;
                Strength = data["attributes"]["strength"]["creation"].ToString();
                Endurance = data["attributes"]["endurance"]["creation"].ToString();
                Coordination = data["attributes"]["coordination"]["creation"].ToString();
                Quickness = data["attributes"]["quickness"]["creation"].ToString();
                Focus = data["attributes"]["focus"]["creation"].ToString();
                Self = data["attributes"]["self"]["creation"].ToString();

                if (data["enableVirindiGlobalInventoryTracking"] != null) {
                    if (data["enableVirindiGlobalInventoryTracking"].ToString().ToLower() == "true") {
                        EnableVGITracking = true;
                    }
                }

                if (data["repeatedlyDelete"] != null) {
                    RepeatedlyDelete = data["repeatedlyDelete"].ToString().ToLower() == "true" ? true : false;
                }
                if (data["levelingTemplate"] != null) {
                    LevelingTemplate = data["levelingTemplate"].ToString();
                }

                OwnsItemsRecoveryCommands.Clear();
                if (data["ownsItemsRecoveryCommands"] != null) {
                    JEnumerable<JToken> recoveryCommands = data["ownsItemsRecoveryCommands"].Children<JToken>();

                    foreach (var token in recoveryCommands) {
                        OwnsItemsRecoveryCommands.Add(token.ToString());
                    }
                }

                DontDeleteIfOwnsItems.Clear();
                if (data["dontDeleteIfOwnsItems"] != null) {
                    JEnumerable<JToken> itemsToCheckFor = data["dontDeleteIfOwnsItems"].Children<JToken>();

                    foreach (var token in itemsToCheckFor) {
                        DontDeleteIfOwnsItems.Add(token.ToString());
                        EnableVGITracking = true;
                    }
                }

                LoginCommands.Clear();
                if (data["loginCommands"] != null) {
                    JEnumerable<JToken> loginCommands = data["loginCommands"].Children<JToken>();

                    foreach (var token in loginCommands) {
                        LoginCommands.Add(token.ToString());
                        Util.WriteToDebugLog($"Found token: {token.ToString()}");
                    }
                }
                else {
                    foreach (var command in Config.DefaultLoginCommands) {
                        LoginCommands.Add(command);
                    }
                }
            }
            catch (Exception ex) {
                Util.LogException(ex);
            }
        }

        public void WriteToLog() {
            Util.WriteToDebugLog($"Egg {Name}:");
            Util.WriteToDebugLog($"\t Race: {Race}");
            Util.WriteToDebugLog($"\t Gender: {Gender}");
            Util.WriteToDebugLog($"\t StartingTown: {StartingTown}");
            Util.WriteToDebugLog($"\t Strength: {Strength}");
            Util.WriteToDebugLog($"\t Endurance: {Endurance}");
            Util.WriteToDebugLog($"\t Coordination: {Coordination}");
            Util.WriteToDebugLog($"\t Quickness: {Quickness}");
            Util.WriteToDebugLog($"\t Focus: {Focus}");
            Util.WriteToDebugLog($"\t Self: {Self}");
        }

        public void Hatch() {
            didError = false;
            isHatched = false;

            screens.Add(new HeritageScreen(this));
            screens.Add(new ProfessionScreen(this));
            screens.Add(new SkillsScreen(this));
            screens.Add(new AppearanceScreen(this));
            screens.Add(new TownScreen(this));
            screens.Add(new SummaryScreen(this));
            screens.Add(new FinishScreen(this));
        }

        internal bool IsHatched() {
            return isHatched;
        }

        internal void ClickNext() {
            Util.WriteToDebugLog("Clicking Next on CharacterCreate screen");
            InputManager.MouseClick(93, 573);
        }

        internal void Think() {
            try {
                if (screens.Count > 0) {
                    screens[0].Think();

                    if (screens[0].DidError()) {
                        Util.WriteToDebugLog("Character create screen errored out, bailing...");
                        screens.Clear();
                        screens.Add(new BailScreen(this));
                        screens[0].Start();
                        didError = true;
                        return;
                    }

                    if (screens[0].IsDone()) {
                        Util.WriteToDebugLog("Moving to next screen");
                        screens[0].Finish();
                        screens.RemoveAt(0);
                        ClickNext();

                        if (screens.Count > 0) {
                            screens[0].Start();
                        }
                    }
                }
                else {
                    Util.WriteToDebugLog("Egg::isHatched is now true");
                    isHatched = true;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}
