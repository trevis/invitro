﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace InVitroFilter.Lib {
    class CharacterTemplate {
        public string Name = "";
        public int Level = 0;

        public Dictionary<AttributeType, int> AttributeTargets = new Dictionary<AttributeType, int>();
        public Dictionary<VitalType, int> VitalTargets = new Dictionary<VitalType, int>();
        public Dictionary<SkillType, int> SkillTargets = new Dictionary<SkillType, int>();

        private JObject data = null;

        private Dictionary<string, string> skillLookups = new Dictionary<string, string>() {
            { "Alchemy", "AlchemySkill" },
            { "Cooking", "CookingSkill" },
            { "Fletching", "FletchingSkill" },
            { "Salvaging", "SkillSalvaging" }
        };

        public static CharacterTemplate FromJSON(string name, string json) {
            try {
                var template = new CharacterTemplate(JObject.Parse(json));
                template.Name = name;
                return template;
            }
            catch (Exception ex) {
                Util.LogException(ex);
                return null;
            }
        }

        public CharacterTemplate(JObject templateData) {
            try {
                data = templateData;

                int.TryParse(data["level"].ToString(), out Level);

                foreach (var attribute in data["attributes"]) {
                    var attributeName = attribute.Path.Split('.')[attribute.Path.Split('.').Length - 1];
                    AttributeType type = (AttributeType)Enum.Parse(typeof(AttributeType), "Base" + char.ToUpper(attributeName[0]) + attributeName.Substring(1));
                    int target;

                    if (int.TryParse(data["attributes"][attributeName]["invested"].ToString(), out target)) {
                        if (target <= 0) continue;
                        Util.WriteToDebugLog($"{attributeName} target is {target} clicks");
                        AttributeTargets.Add(type, target);
                    }
                }

                foreach (var vital in data["vitals"]) {
                    var vitalName = vital.Path.Split('.')[vital.Path.Split('.').Length - 1];
                    VitalType type = (VitalType)Enum.Parse(typeof(VitalType), "Base" + char.ToUpper(vitalName[0]) + vitalName.Substring(1));
                    int target;

                    if (int.TryParse(data["vitals"][vitalName]["invested"].ToString(), out target)) {
                        if (target <= 0) continue;
                        Util.WriteToDebugLog($"{vitalName} target is {target} clicks");
                        VitalTargets.Add(type, target);
                    }
                }

                foreach (var skill in data["skills"]) {
                    var originalSkillName = skill.Path.Split('.')[skill.Path.Split('.').Length - 1];
                    if (originalSkillName.ToLower() == "summoning") continue;
                    var skillName = char.ToUpper(originalSkillName[0]) + Util.UnderscoreToCamelCase(originalSkillName.Substring(1));
                    if (skillLookups.ContainsKey(skillName)) skillName = skillLookups[skillName];
                    SkillType type = (SkillType)Enum.Parse(typeof(SkillType), "Base" + skillName);
                    int target;

                    if (int.TryParse(data["skills"][originalSkillName]["invested"].ToString(), out target)) {
                        if (target <= 0) continue;
                        Util.WriteToDebugLog($"{skillName} target is {target} clicks");
                        SkillTargets.Add(type, target);
                    }
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}
