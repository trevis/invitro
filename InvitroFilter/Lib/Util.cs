﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Decal.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace InVitroFilter.Lib {
    class Util {
        static FileService fileService = null;

        public static string Name = "InVitro";

        internal static string GetDataDirectory() {
            return Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\Decal Plugins\" + Name + @"\"; ;
        }

        public static void CreatePluginDirectories() {
            Directory.CreateDirectory(GetDataDirectory());
        }

        public static string GetLogDirectory() {
            return Path.Combine(GetDataDirectory(), "logs");
        }

        public static string GetEggsDirectory() {
            return Path.Combine(GetDataDirectory(), "eggs");
        }

        internal static void WriteToChat(string message) {
            try {
                CoreManager.Current.Actions.AddChatText("[" + Name + "] " + message, 5);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public static string GetTemplatesDirectory() {
            return Path.Combine(GetDataDirectory(), "templates");
        }

        public static FileService GetFileService() {
            if (fileService == null) fileService = CoreManager.Current.Filter<FileService>();
            return fileService;
        }

        internal static void AddToChatBox(string v) {
            DispatchChatToBoxWithPluginIntercept(v);
        }

        public static string GetEggsDirectory(string serverName, string accountName) {
            var path = GetEggsDirectory();

            path = Path.Combine(path, serverName);
            path = Path.Combine(path, accountName);

            return path;
        }

        public static string GetAssemblyDirectory() {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }

        public static void CreateDataDirectories() {
            System.IO.Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\Decal Plugins\");
            System.IO.Directory.CreateDirectory(GetDataDirectory());
            System.IO.Directory.CreateDirectory(GetLogDirectory());
            System.IO.Directory.CreateDirectory(GetEggsDirectory());
            System.IO.Directory.CreateDirectory(GetTemplatesDirectory());
        }

        public static void LogException(Exception ex) {
            try {

                using (StreamWriter writer = new StreamWriter(Path.Combine(Util.GetDataDirectory(), "exceptions.txt"), true)) {
                    writer.WriteLine("============================================================================");
                    writer.WriteLine(DateTime.Now.ToString());
                    writer.WriteLine("Error: " + ex.Message);
                    writer.WriteLine("Source: " + ex.Source);
                    writer.WriteLine("Stack: " + ex.StackTrace);
                    if (ex.InnerException != null) {
                        writer.WriteLine("Inner: " + ex.InnerException.Message);
                        writer.WriteLine("Inner Stack: " + ex.InnerException.StackTrace);
                    }
                    writer.WriteLine("============================================================================");
                    writer.WriteLine("");
                    writer.Close();
                    Util.WriteToChat("============================================================================");
                    Util.WriteToChat(DateTime.Now.ToString());
                    Util.WriteToChat("Error: " + ex.Message);
                    Util.WriteToChat("Source: " + ex.Source);
                    Util.WriteToChat("Stack: " + ex.StackTrace);
                    if (ex.InnerException != null) {
                        Util.WriteToChat("Inner: " + ex.InnerException.Message);
                        Util.WriteToChat("Inner Stack: " + ex.InnerException.StackTrace);
                    }
                    Util.WriteToChat("============================================================================");
                    Util.WriteToChat("");
                }
            }
            catch {
            }
        }

        public static string egg = "debug";

        public static void WriteToDebugLog(string message) {
            if (egg == "debug") return;
            WriteToLogFile(egg, message, true);
        }

        public static void WriteToLogFile(string logName, string message, bool addTimestamp = false) {
            var today = DateTime.Now.ToString("yyyy-MM-dd");
            var logFileName = String.Format("{0}.{1}.txt", logName, today);

            if (addTimestamp) {
                message = String.Format("{0} {1}", DateTime.Now.ToString("yy/MM/dd H:mm:ss"), message);
            }

            File.AppendAllText(Path.Combine(Util.GetLogDirectory(), logFileName), message + Environment.NewLine);
        }


        [DllImport("Decal.dll")]
        static extern int DispatchOnChatCommand(ref IntPtr str, [MarshalAs(UnmanagedType.U4)] int target);

        public static bool Decal_DispatchOnChatCommand(string cmd) {
            IntPtr bstr = Marshal.StringToBSTR(cmd);

            try {
                bool eaten = (DispatchOnChatCommand(ref bstr, 1) & 0x1) > 0;

                return eaten;
            }
            finally {
                Marshal.FreeBSTR(bstr);
            }
        }

        public static void DispatchChatToBoxWithPluginIntercept(string cmd) {
            if (!Decal_DispatchOnChatCommand(cmd))
                CoreManager.Current.Actions.InvokeChatParser(cmd);
        }

        public static int GetXPNeededToRaiseAttribute(Decal.Adapter.Wrappers.AttributeType attribute, int count) {
            var clicks = CoreManager.Current.Actions.AttributeClicks[attribute];
            var table = GetFileService().LevelTables.AttributeXP;
            var xpNeeded = 0;

            for (var i = 1; i <= count && table.Length > (clicks + count); i++) {

                xpNeeded += (int)(table[clicks + i] - table[clicks + i - 1]);
            }

            return xpNeeded;
        }

        public static int GetXPNeededToRaiseVital(Decal.Adapter.Wrappers.VitalType vital, int count) {
            var clicks = CoreManager.Current.Actions.VitalClicks[vital];
            var table = GetFileService().LevelTables.VitalXP;
            var xpNeeded = 0;

            for (var i = 1; i <= count && table.Length > (clicks + count); i++) {
                xpNeeded += (int)(table[clicks + i] - table[clicks + i - 1]);
            }

            return xpNeeded;
        }

        public static int GetXPNeededToRaiseSkill(Decal.Adapter.Wrappers.SkillType skill, int count) {
            var clicks = CoreManager.Current.Actions.SkillClicks[skill];
            RawTable<long> table = null;
            var xpNeeded = 0;

            switch (CoreManager.Current.Actions.SkillTrainLevel[skill]) {
                case 2: // trained
                    table = GetFileService().LevelTables.TrainedSkillXP;
                    break;

                case 3: // specialized
                    table = GetFileService().LevelTables.SpecializedSkillXP;
                    break;
            }

            if (table == null) return 0;
            for (var i = 1; i <= count && table.Length > (clicks + count + 1); i++) {
                var currentSkillXP = table[clicks + i - 1];
                if (i == 1) {
                    currentSkillXP = CoreManager.Current.CharacterFilter.Skills[(CharFilterSkillType)skill - 50].XP;
                }
                xpNeeded += (int)(table[clicks + i] - currentSkillXP);
            }

            return xpNeeded;
        }


        public static string UnderscoreToCamelCase(string name) {
            if (string.IsNullOrEmpty(name) || !name.Contains("_")) {
                return name;
            }
            string[] array = name.Split('_');
            for (int i = 0; i < array.Length; i++) {
                string s = array[i];
                string first = string.Empty;
                string rest = string.Empty;
                if (s.Length > 0) {
                    first = Char.ToUpperInvariant(s[0]).ToString();
                }
                if (s.Length > 1) {
                    rest = s.Substring(1).ToLowerInvariant();
                }
                array[i] = first + rest;
            }
            string newname = string.Join("", array);
            if (newname.Length > 0) {
                newname = Char.ToLowerInvariant(newname[0]) + newname.Substring(1);
            }
            else {
                newname = name;
            }
            return newname;
        }
    }
}
