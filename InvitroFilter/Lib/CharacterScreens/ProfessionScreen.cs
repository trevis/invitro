﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InVitroFilter.Lib.CharacterScreens {
    class ProfessionScreen : CharacterScreen {
        private bool didClickCustomProfession = false;
        private Dictionary<string, int> skillYOffsets = new Dictionary<string, int>();

        public ProfessionScreen(Egg egg) : base(egg) {
            skillYOffsets.Add("strength", 230);
            skillYOffsets.Add("endurance", 275);
            skillYOffsets.Add("coordination", 320);
            skillYOffsets.Add("quickness", 365);
            skillYOffsets.Add("focus", 410);
            skillYOffsets.Add("self", 455);
        }

        public override void Start() {
            Util.WriteToDebugLog("Starting Profession Screen");
        }

        public override void Finish() {
            Util.WriteToDebugLog("Finishing Profession Screen");
        }

        public void ClickCustomProfession() {
            Util.WriteToDebugLog("Clicking custom profession");
            InputManager.MouseClick(116, 130);
            didClickCustomProfession = true;
        }

        bool needsClick = true;
        bool needsDeletes = true;
        bool needsValue = true;

        DateTime lastThought = DateTime.UtcNow;

        public override void Think() {
            lastThought = DateTime.UtcNow;

            if (!didClickCustomProfession) {
                ClickCustomProfession();
                return;
            }

            if (skillYOffsets.Keys.Count > 0) {
                var e = skillYOffsets.GetEnumerator();
                if (e.MoveNext()) {
                    var current = e.Current;
                    if (current.Key == null) return;
                    var val = egg.Data["attributes"][current.Key.ToString()]["creation"].ToString();

                    if (needsClick && needsDeletes && needsValue) {
                        Util.WriteToDebugLog($"{current.Key} @ {current.Value} setting to {val}");
                    }

                    if (needsClick) {
                        needsClick = false;
                        InputManager.MouseClick(759, current.Value);
                        Util.WriteToDebugLog("Clicking into attribute field");
                        return;
                    }
                    if (needsDeletes) {
                        needsDeletes = false;
                        Util.WriteToDebugLog("Clearing field");
                        InputManager.ClearTextField();
                        return;
                    }
                    if (needsValue) {
                        needsValue = false;
                        Util.WriteToDebugLog("Writing new value: " + val);
                        InputManager.SendKeys(val);
                        return;
                    }

                    needsClick = true;
                    needsDeletes = true;
                    needsValue = true;

                    skillYOffsets.Remove(current.Key);

                    return;

                }
            }

            isDone = true;
        }
    }
}
