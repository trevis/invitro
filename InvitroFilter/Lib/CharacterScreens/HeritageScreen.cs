﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Text;

namespace InVitroFilter.Lib.CharacterScreens {
    class HeritageScreen : CharacterScreen {
        private Dictionary<string, int> HeritageYOffsets = new Dictionary<string, int>();
        private int HeritageXOffset = 120;
        private string race;

        public HeritageScreen(Egg egg) : base(egg) {
            HeritageYOffsets.Add("Aluvian", 88);
            HeritageYOffsets.Add("Gharu'ndim", 124);
            HeritageYOffsets.Add("Sho", 158);
            HeritageYOffsets.Add("Viamontian", 192);
            HeritageYOffsets.Add("Umbraen", 230);
            HeritageYOffsets.Add("Penumbraen", 260);
            HeritageYOffsets.Add("Gear Knight", 298);
            HeritageYOffsets.Add("Undead", 335);
            HeritageYOffsets.Add("Empyrean", 368);
            HeritageYOffsets.Add("Aun Tumerok", 402);
            HeritageYOffsets.Add("Lugian", 438);
            HeritageYOffsets.Add("Olthoi Soldier", 475);
            HeritageYOffsets.Add("Olthoi Spitter", 509);

            race = egg.Data["race"].ToString();
        }

        public override void Start() {
            Util.WriteToDebugLog("Starting Heritage Screen");
        }

        public override void Finish() {
            Util.WriteToDebugLog("Finishing Heritage Screen");
        }

        public override void Think() {
            if (!HeritageYOffsets.ContainsKey(race)) {
                Util.WriteToDebugLog($"Error looking up heritage offset from race: {egg.Race}");
                return;
            }

            var yOffset = HeritageYOffsets[race];

            Util.WriteToDebugLog($"Selecting heritage: {race}");

            InputManager.MouseClick(HeritageXOffset, yOffset);

            isDone = true;
        }
    }
}
