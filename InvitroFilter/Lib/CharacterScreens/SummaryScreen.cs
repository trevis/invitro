﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InVitroFilter.Lib.CharacterScreens {
    class SummaryScreen : CharacterScreen {
        bool hasTypedName = false;
        bool didClickFinish = false;

        public SummaryScreen(Egg egg) : base(egg) {

        }

        public override void Start() {
            Util.WriteToDebugLog("Starting Summary Screen");
        }

        public override void Finish() {
            Util.WriteToDebugLog("Finishing Summary Screen");
        }

        public override void Think() {
            if (!hasTypedName) {
                hasTypedName = true;
                Util.WriteToDebugLog($"Inputting Character name: {egg.Name}");
                InputManager.ClearTextField(10);
                InputManager.SendKeys(egg.Name);
                return;
            }

            // Click Finish
            if (!didClickFinish) {
                didClickFinish = true;
                InputManager.MouseClick(720, 570);
                return;
            }


            // Click Finish
            InputManager.MouseClick(720, 570);

            isDone = true;
        }
    }
}
