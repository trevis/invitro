﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InVitroFilter.Lib.CharacterScreens {
    struct MouseClick {
        public int X;
        public int Y;

        public MouseClick(int x, int y) {
            X = x;
            Y = y;
        }
    }
    class BailScreen : CharacterScreen {
        List<MouseClick> actions = new List<MouseClick>();

        public BailScreen(Egg egg) : base(egg) {

        }

        public override void Start() {
            Util.WriteToDebugLog("Starting Bail Screen");
            InputManager.KillClient();
        }

        public override void Finish() {
            Util.WriteToDebugLog("Finishing Bail Screen");
        }

        public override void Think() {

            isDone = true;
        }
    }
}
