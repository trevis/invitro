﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InVitroFilter.Lib.CharacterScreens {
    struct TownPosition {
        public int X;
        public int Y;

        public TownPosition(int x, int y) {
            X = x;
            Y = y;
        }
    }

    class TownScreen : CharacterScreen {
        private bool hasSelectedTown = false;

        private Dictionary<string, TownPosition> TownPositions = new Dictionary<string, TownPosition>();

        public TownScreen(Egg egg) : base(egg) {
            TownPositions.Add("holtburg", new TownPosition(605, 208));
            TownPositions.Add("sanamar", new TownPosition(377, 141));
            TownPositions.Add("shoushi", new TownPosition(697, 392));
            TownPositions.Add("yaraq", new TownPosition(544, 354));
        }

        public override void Start() {
            Util.WriteToDebugLog("Starting Town Screen");
        }

        public override void Finish() {
            Util.WriteToDebugLog("Finishing Town Screen");
        }

        public override void Think() {
            if (!hasSelectedTown) {
                hasSelectedTown = true;
                string town = Config.DefaultStartingTown.ToLower();

                if (egg.Data["startingTown"] != null) {
                    town = egg.Data["startingTown"].ToString().ToLower();
                    Util.WriteToDebugLog($"Using custom StartingTown: {town}");
                }

                if (!TownPositions.ContainsKey(town)) {
                    Util.WriteToDebugLog($"StartingTown not found: {town}, going to use default holtburg");
                    town = "holtburg";
                }

                InputManager.MouseClick(TownPositions[town].X, TownPositions[town].Y);
                Util.WriteToDebugLog($"Clicking Starting Town: {town}");
                return;
            }

            isDone = true;
        }
    }
}
