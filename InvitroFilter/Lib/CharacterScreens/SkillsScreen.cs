﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace InVitroFilter.Lib.CharacterScreens {
    class SkillsScreen : CharacterScreen {
        private List<string> SpecializedSkills = new List<string>();
        private List<string> TrainedSkills = new List<string>();
        private List<string> UseableSkills = new List<string>();
        private List<string> UnuseableSkills = new List<string>();

        private List<string> SkillsToCheck = new List<string>();
        private List<string> SkillList = new List<string>();

        private int scrollPosition = 0;

        private const int MAX_VISIBLE = 12;
        private int LIST_LENGTH;

        public SkillsScreen(Egg egg) : base(egg) {
            TrainedSkills.Add("Arcane Lore");
            TrainedSkills.Add("Jump");
            TrainedSkills.Add("Loyalty");
            TrainedSkills.Add("Magic Defense");
            TrainedSkills.Add("Run");
            TrainedSkills.Add("Salvaging");

            UseableSkills.Add("Armor Tinkering");
            UseableSkills.Add("Assess Creature");
            UseableSkills.Add("Assess Person");
            UseableSkills.Add("Deception");
            UseableSkills.Add("Dual Wield");
            UseableSkills.Add("Finesse Weapons");
            UseableSkills.Add("Heavy Weapons");
            UseableSkills.Add("Item Tinkering");
            UseableSkills.Add("Leadership");
            UseableSkills.Add("Light Weapons");
            UseableSkills.Add("Magic Item Tinkering");
            UseableSkills.Add("Melee Defense");
            UseableSkills.Add("Missile Defense");
            UseableSkills.Add("Missile Weapons");
            UseableSkills.Add("Shield");
            UseableSkills.Add("Two Handed Combat");
            UseableSkills.Add("Weapon Tinkering");

            UnuseableSkills.Add("Alchemy");
            UnuseableSkills.Add("Cooking");
            UnuseableSkills.Add("Creature Enchantment");
            UnuseableSkills.Add("Dirty Fighting");
            UnuseableSkills.Add("Fletching");
            UnuseableSkills.Add("Healing");
            UnuseableSkills.Add("Item Enchantment");
            UnuseableSkills.Add("Life Magic");
            UnuseableSkills.Add("Lockpick");
            UnuseableSkills.Add("Mana Conversion");
            UnuseableSkills.Add("Recklessness");
            UnuseableSkills.Add("Sneak Attack");
            UnuseableSkills.Add("Summoning");
            UnuseableSkills.Add("Void Magic");
            UnuseableSkills.Add("War Magic");

            SkillsToCheck.AddRange(TrainedSkills);
            SkillsToCheck.AddRange(UseableSkills);
            SkillsToCheck.AddRange(UnuseableSkills);

            BuildSkillList();

            LIST_LENGTH = SkillsToCheck.Count + 4 /* headers */;
        }

        public override void Start() {
            Util.WriteToDebugLog("Starting Skills Screen");
        }

        public override void Finish() {
            Util.WriteToDebugLog("Finishing Skills Screen");
        }

        public void BuildSkillList() {
            SpecializedSkills.Sort();
            TrainedSkills.Sort();
            UseableSkills.Sort();
            UnuseableSkills.Sort();

            SkillList.Clear();
            SkillList.Add("[Specialized Skills]");
            SkillList.AddRange(SpecializedSkills);
            SkillList.Add("[Trained Skills]");
            SkillList.AddRange(TrainedSkills);
            SkillList.Add("[Useable Untrained Skills]");
            SkillList.AddRange(UseableSkills);
            SkillList.Add("[Unuseable Untrained Skills]");
            SkillList.AddRange(UnuseableSkills);
        }

        public bool ScrollTo(string skill) {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            skill = textInfo.ToTitleCase(skill.Replace("_", ""));

            var index = SkillList.IndexOf(skill);

            if (index < scrollPosition || index > scrollPosition + MAX_VISIBLE - 1) {
                var scrollUp = (index < scrollPosition);

                if (scrollUp) {
                    InputManager.MouseClick(465, 115);
                    scrollPosition--;
                }
                else {
                    InputManager.MouseClick(465, 405);
                    scrollPosition++;
                }

                return false;
            }

            return true;
        }

        public int GetSkillYOffset(string skill) {
            int offset = 110; /* starting list y offset */

            offset += (SkillList.IndexOf(skill) - scrollPosition) * 26 /* skill row height */;

            return offset;
        }

        public void TrainSkill(string skill) {
            InputManager.MouseClick(375, GetSkillYOffset(skill));

            if (TrainedSkills.Contains(skill)) {
                Util.WriteToDebugLog($"Clicking to specialize skill '{skill}' (from trained) @ x:375 y:{GetSkillYOffset(skill)}");
                TrainedSkills.Remove(skill);
                SpecializedSkills.Add(skill);
            }
            else if (UseableSkills.Contains(skill)) {
                Util.WriteToDebugLog($"Clicking to train skill '{skill}' (from useable) @ x:375 y:{GetSkillYOffset(skill)}");
                UseableSkills.Remove(skill);
                TrainedSkills.Add(skill);
            }
            else if (UnuseableSkills.Contains(skill)) {
                Util.WriteToDebugLog($"Clicking to train skill '{skill}' (from unuseable) @ x:375 y:{GetSkillYOffset(skill)}");
                UnuseableSkills.Remove(skill);
                TrainedSkills.Add(skill);
            }

            BuildSkillList();
        }

        public override void Think() {
            if (SkillsToCheck.Count > 0) {
                var name = SkillsToCheck[0].Replace(" ", "_").ToLower();
                var training = egg.Data["skills"][name]["training"].ToString();

                if (training == "specialized" && !SpecializedSkills.Contains(SkillsToCheck[0])) {
                    if (ScrollTo(SkillsToCheck[0])) {
                        TrainSkill(SkillsToCheck[0]);

                        if (TrainedSkills.Contains(SkillsToCheck[0])) {
                            SkillsToCheck.Add(SkillsToCheck[0]);
                        }

                        SkillsToCheck.RemoveAt(0);
                    }
                    return;
                }

                if (training == "trained" && !TrainedSkills.Contains(SkillsToCheck[0])) {
                    if (ScrollTo(SkillsToCheck[0])) {
                        TrainSkill(SkillsToCheck[0]);
                        SkillsToCheck.RemoveAt(0);
                    }
                    return;
                }

                SkillsToCheck.RemoveAt(0);
                return;
            }

            isDone = true;
        }
    }
}
