﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using Decal.Adapter;
using System.IO;

namespace InVitroFilter.Lib.CharacterScreens {
    enum CharGenResponseType {
        OK = 0x00000001,
        NameInUse = 0x00000003,
        NameBanned = 0x00000004,
        Corrupt1 = 0x00000005,
        Corrupt2 = 0x00000006,
        AdminPrivilegeDenied = 0x00000007
    }

    class FinishScreen : CharacterScreen {
        bool hasLoggedIn = false;
        List<string> loginCommands = new List<string>();
        DateTime firstThought = DateTime.UtcNow;
        private bool gotGoodResponse;

        public FinishScreen(Egg egg) : base(egg) {

        }

        public override void Start() {
            Util.WriteToDebugLog("Starting Finish Screen");

            DateTime firstThought = DateTime.UtcNow;
            
            CoreManager.Current.CharacterFilter.LoginComplete += CharacterFilter_LoginComplete;
            CoreManager.Current.EchoFilter.ServerDispatch += new EventHandler<NetworkMessageEventArgs>(EchoFilter_ServerDispatch);

            loginCommands = new List<string>(egg.LoginCommands);

            if (loginCommands.Count == 0) loginCommands = new List<string>(Config.DefaultLoginCommands);

            if (egg.NeedsToReturnItems) {
                loginCommands = egg.OwnsItemsRecoveryCommands;
            }

            Util.WriteToDebugLog("Login Commands are: ");

            foreach (var cmd in loginCommands) {
                Util.WriteToDebugLog("\t" + cmd);
            }
        }

        public override void Finish() {
            Util.WriteToDebugLog("Finishing Finish Screen");
            CoreManager.Current.CharacterFilter.LoginComplete += CharacterFilter_LoginComplete;
            CoreManager.Current.EchoFilter.ServerDispatch -= new EventHandler<NetworkMessageEventArgs>(EchoFilter_ServerDispatch);
        }

        private void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            try {
                hasLoggedIn = true;
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        void EchoFilter_ServerDispatch(object sender, NetworkMessageEventArgs e) {
            try {
                switch (e.Message.Type) {
                    case 0xF643:
                        var response = BitConverter.ToUInt32(e.Message.RawData, 4);
                        if (response == (uint)CharGenResponseType.OK) {
                            gotGoodResponse = true;
                        }
                        else {
                            isDone = true;
                            didError = true;

                            var invalidEggsPath = Path.Combine(Util.GetEggsDirectory(egg.Server, egg.Account), "aborts");

                            if (!Directory.Exists(invalidEggsPath)) {
                                Directory.CreateDirectory(invalidEggsPath);
                            }

                            switch ((CharGenResponseType)response) {
                                case CharGenResponseType.NameInUse:
                                    Util.WriteToDebugLog($"The name {egg.Name} is already in use on this server");
                                    break;
                                case CharGenResponseType.NameBanned:
                                    Util.WriteToDebugLog($"The name {egg.Name} is banned on this server");
                                    break;
                                default:
                                    Util.WriteToDebugLog($"Unable to create user {egg.Name} on this server");
                                    break;
                            }

                            if (!egg.RepeatedlyDelete) {
                                Util.WriteToDebugLog($"Moving egg file to aborts directory");
                                var newPath = Path.Combine(invalidEggsPath, $"{egg.Name}.json");

                                if (File.Exists(newPath)) File.Delete(newPath);

                                File.Move(egg.Path, newPath);
                            }
                        }
                        break;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        DateTime lastThought = DateTime.UtcNow;

        public override void Think() {
            if (isDone) return;

            if (!gotGoodResponse && DateTime.UtcNow - firstThought > TimeSpan.FromSeconds(10)) {
                isDone = true;
                didError = true;

                Util.WriteToDebugLog($"There was an error creating {egg.Name}, will try again...");
            }

            if (!hasLoggedIn) return;

            if (DateTime.UtcNow - lastThought < TimeSpan.FromMilliseconds(200)) return;
            lastThought = DateTime.UtcNow;

            if (loginCommands.Count > 0) {
                var command = loginCommands[0].Replace("<[RANDOM]>", DateTime.UtcNow.ToFileTimeUtc().ToString());
                Util.AddToChatBox(command);
                Util.WriteToDebugLog("Issuing login command: " + command);
                loginCommands.RemoveAt(0);
                return;
            }
            else {
                isDone = true;
            }
        }
    }
}
