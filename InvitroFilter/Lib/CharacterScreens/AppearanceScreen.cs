﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InVitroFilter.Lib.CharacterScreens {
    class AppearanceScreen : CharacterScreen {
        bool hasChosenGender = false;

        public AppearanceScreen(Egg egg) : base(egg) {

        }

        public override void Start() {
            Util.WriteToDebugLog("Starting Appearance Screen");
        }

        public override void Finish() {
            Util.WriteToDebugLog("Finishing Appearance Screen");
        }

        public override void Think() {
            if (!hasChosenGender) {
                hasChosenGender = true;

                Util.WriteToDebugLog($"Selecting gender: {egg.Gender}");

                if (egg.Gender.ToLower() == "male") {
                    InputManager.MouseClick(178, 108);
                }
                else {
                    InputManager.MouseClick(105, 105);
                }

                return;
            }

            isDone = true;
        }
    }
}
