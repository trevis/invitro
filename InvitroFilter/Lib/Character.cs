﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InVitroFilter.Lib {
    class Character {
        public int Id;
        public int Slot;
        public string Name;
        public TimeSpan DeleteTimeout;

        private const int CharacterListXOffset = 86;
        private const int CharacterListYOffset = 218;
        private const int CharacterSlotHeight = 27;

        public Character(int id, string name, int timeout, int index) {
            Id = id;
            Name = name;
            DeleteTimeout = TimeSpan.FromSeconds(timeout);
            Slot = index;
        }

        public void Click() {
            InputManager.MouseClick(CharacterListXOffset + 10, CharacterListYOffset + 5 + (CharacterSlotHeight * Slot));
        }
    }
}
