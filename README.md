# InVitro (Beta)
A character creation / xp spending filter for AC Decal.
![](https://i.gyazo.com/a6600ced0d55158340d11c35ff84e827.mp4)

# Instructions
* Install [InVitroInstaller-1.3.1.0.exe](https://gitlab.com/trevis/invitro/uploads/80e84b5ae19e166f0578a419b5c22750/InVitroInstaller-1.3.1.0.exe)
* Go to [planner.treestats.net](https://planner.treestats.net/) and export a character json file
* Save the generated json file to `documents/Decal Plugins/InVitro/eggs/<server>/<account_name>/<character_name>.json` (making sure to replace `<server>`, `<account_name>`, and `<character_name>` in the path above)
* Next time your game is at the character list screen, if there is an egg json file without a character created, it will automatically attempt to create it.
* If you want to have your character automatically spend xp as it earns it, you need to define a template.  See the [Templates](#templates) section

# More Info
* The default loginCommand is set to `/mt logoff` to facilitate creating multiple characters on the same account. You can change it by editing `Documents\Decal Plugins\InVitro\config.xml`.
* You can edit default starting town and login commands in `Document/Decal Plugins/InVitro/config.xml`
* If you want to adjust a starting town or login commands per character, edit the character json file and add the following top level attributes:
```
'startingTown': 'sanamar',
'loginCommands': [
	'/vt echo This is a custom login command',
	'/mt jump'
]
```
* You can optionally have a character get deleted and remade repeatedly if you set the top level attribute `'repeatedlyDelete': true`. When at the login screen, it will first delete any existing characters with the repeatedlyDelete attribute set to true.  After deleting it will go through its normal creation stage.
* If there is a problem creating a character, the json file gets moved an `aborts/` directory so that the bot doesn't get stuck looping on it. Note: if repeatedlyDelete is set to true the egg will never abort.

# Templates
Templates are a way to have your character automatically spend xp as they earn it. 

* Go to [planner.treestats.net](https://planner.treestats.net/) and export at level 10, and level 50 (any levels are ok).  
* Create a new directory `documents/Decal Plugins/InVitro/templates/<templatename>/`
* Save your template files to the new directory, it doesnt matter what they are named.
* Open your character egg json file, and add a top level attribute `"levelingTemplate": "<templatename>"` (whatever you named the directory earlier)
* As you level, the lowest level template that is not less than your current character will be used.
* Experience will be dumped into the lowest costing skills first. You can define a level 10 template where all stats are dumped into health, followed by a level 20 template where you add points into war magic, melee defense, etc.  This will let you prioritize raising different skills as you level.